FROM node:10

RUN userdel -f node \
 && groupadd --gid 1000 app \
 && useradd --uid 1000 --gid app --create-home app \
 && mkdir -p /app \
 && chown -R 1000:1000 /app

USER app
WORKDIR /app

COPY --chown=app:app ./package*.json /app/
COPY --chown=app:app ./bower.json /app/
RUN npm i
RUN cd ./node_modules/run-sequence/ && npm i chalk@1.1.3 --save

CMD ["npm", "start"]
COPY --chown=app:app ./css /app/css
COPY --chown=app:app ./data /app/data
COPY --chown=app:app ./imgs /app/imgs
COPY --chown=app:app ./js /app/js
COPY --chown=app:app ./views /app/views
COPY --chown=app:app ./gulpfile.js ./index.html ./server.js /app/
